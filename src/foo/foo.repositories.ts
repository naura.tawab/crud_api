import { FooEntity } from './foo.entities';
import { getConnection } from 'typeorm';

interface FooRepository {
  getFoos: () => Promise<FooEntity[]>;
}

const getFoos = async () => {
  return await getConnection()
    .getRepository(FooEntity)
    .find();
};

const fooRepository: FooRepository = {
  getFoos
};

export { FooRepository, fooRepository };
