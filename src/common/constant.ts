const ConfigParameter = {
  db: 'db'
};

const Tracing = {
  TRACER_SESSION: 'TRACER_SESSION',
  TRANSACTION_ID: 'x-request-id'
};

export { ConfigParameter, Tracing };
