import userService from '../user.service';
import userRepository from '../user.repository';
import { ERROR_CODE } from '../../common/errors';
import { AppError } from '../../errors/AppError';

jest.mock('../user.repository');

describe('userService', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('getUsers', () => {
    it('should return repository get result', async () => {
      const usersTest = [
        {
          name: 'test'
        }
      ];
      userRepository.get.mockResolvedValueOnce(usersTest);
      const users = await userService.getUsers();
      expect(users).toEqual(usersTest);
    });
  });

  describe('createUser', () => {
    it('should throw error if repository find existing user with same name', async () => {
      const userInfo = {
        name: 'test',
        age: 12
      };
      userRepository.getFirstByName.mockResolvedValueOnce({
        name: 'test'
      });
      try {
        await userService.createUser(userInfo);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect(e.errorCode).toEqual(ERROR_CODE.USER_NAME_EXISTED);
      } finally {
        expect(userRepository.create).not.toBeCalled();
      }
    });

    it('should call repository create user', async () => {
      const userInfo = {
        name: 'test',
        age: 12
      };
      userRepository.getFirstByName.mockResolvedValueOnce(null);
      await userService.createUser(userInfo);
      expect(userRepository.create).toBeCalledWith(userInfo);
    });
  });
});
