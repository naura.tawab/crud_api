import hapi from '@hapi/hapi';
import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import { UserValidator, UserResponseValidator } from './user.validator';

export type IUser = Joi.extractType<typeof UserValidator>;

export type IUserResponse = Joi.extractType<typeof UserResponseValidator>;

export interface IUserRequest extends hapi.Request {
  payload: IUser;
}
