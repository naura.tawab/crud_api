import mongoose, { Schema, Document, Model } from 'mongoose';
import { IUser } from './user.interface';

export type UserDocument = IUser & Document;

const UserSchema: Schema = new Schema({
  name: {
    required: true,
    type: String
  },
  age: Number
});

export const UserModel: Model<UserDocument> = mongoose.model(
  'User',
  UserSchema
);
