import Hapi from '@hapi/hapi';
import { getNamespace } from 'cls-hooked';

import logger from '../logger';
import { HttpResponse } from '../common/HttpResponse';
import { AppError } from '../errors/AppError';
import { Tracing } from '../common/constant';

const documentPathRegex = /^\/(documentation|swagger)/;

const handleHapiResponse = (
  hapiRequest: Hapi.Request,
  hapiResponse: Hapi.ResponseToolkit
) => {
  // ignore document ui path
  if (documentPathRegex.test(hapiRequest.url.pathname))
    return hapiResponse.continue;

  const httpResponse = new HttpResponse<any>();

  const responseData = hapiResponse.request.response;
  if (responseData instanceof Error) {
    // parse raw error not coming from server handler, ex: joi validation
    if (!responseData.isServer) {
      httpResponse.fail(
        {
          message: responseData.output.payload.message,
          code: responseData.output.payload.error
        },
        responseData.output.statusCode
      );
    }

    logger.error(responseData.message, responseData);
    if (responseData instanceof AppError) {
      const errors = responseData.getErrors();
      httpResponse.fail(
        {
          message: errors.message,
          code: responseData.errorCode,
          errors: errors.errors
        },
        errors.statusCode
      );
    } else {
      httpResponse.fail(
        {
          message: responseData.output.payload.message,
          code: responseData.output.payload.error
        },
        responseData.output.statusCode
      );
    }
  } else {
    httpResponse.success(responseData.source, responseData.statusCode);
  }

  let response = hapiResponse
    .response(httpResponse.getBody())
    .code(httpResponse.getStatusCode());
  if (getNamespace(Tracing.TRACER_SESSION)) {
    const transactionId = getNamespace(Tracing.TRACER_SESSION).get(
      Tracing.TRANSACTION_ID
    );
    response.header(Tracing.TRANSACTION_ID, transactionId);
  }
  return response;
};

const responseWrapper: Hapi.Plugin<{}> = {
  name: 'responseWrapper',
  version: '1.0.0',
  register: (server: Hapi.Server) => {
    server.ext('onPreResponse', handleHapiResponse);
  },
  once: true
};

export default responseWrapper;
