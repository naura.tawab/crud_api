import hapi from '@hapi/hapi';
import * as Inert from '@hapi/inert';
import * as Vision from '@hapi/vision';
import { getNamespace } from 'cls-hooked';

import { connectMongo } from './common/mongoDb';
import errorHandler from './common/handleValidationErrors';
// import sqlDb from './common/sqlDb'; // Commenting for now will revert
import Swagger from './plugins/swagger';
import Good from './plugins/good';
import ResponseWrapper from './plugins/responseWrapper';
import RequestWrapper from './plugins/requestWrapper';
import { Tracing } from './common/constant';

import { routes } from './routes';
import logger from './logger';
import { config } from './config';

const { port, host } = config.get('server');

const createServer = async () => {
  const server = new hapi.Server({
    port,
    host,
    routes: {
      validate: {
        options: {
          abortEarly: false
        },
        failAction: errorHandler
      }
    }
  });
  // Register routes
  server.route(routes);

  server.events.on('response', request => {
    const session = getNamespace(Tracing.TRACER_SESSION);
    // @ts-ignore suggest any better approach?
    const clsCtx = request.app[Tracing.TRACER_SESSION].context;
    session.exit(clsCtx);
  });

  const plugins: any[] = [
    Inert,
    Vision,
    Swagger,
    Good,
    ResponseWrapper,
    RequestWrapper
  ];
  await server.register(plugins);

  return server;
};

export const init = async () => {
  await connectMongo();
  const server = await createServer();
  await server
    .initialize()
    .then(() =>
      logger.info(`server started at ${server.info.host}:${server.info.port}`)
    );
  return server;
};

export const start = async (module: NodeModule) => {
  if (!module.parent) {
    // Commenting for now will revert
    // await sqlDb.init();
    logger.info('Start server');
    await init()
      .then(server => server.start())
      .catch(err => {
        logger.error('Server cannot start', err);
        logger.onFinished(() => {
          process.exit(1);
        });
      });
  }
};

start(module);
